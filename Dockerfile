FROM ubuntu:latest

RUN apt-get update && apt-get install -y texlive-latex-base texlive-latex-extra texlive-fonts-recommended python3 make
RUN apt-get install -y tex4ht calibre

